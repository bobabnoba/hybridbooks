CREATE TABLE users
(
    id        serial PRIMARY KEY,
    email     varchar(40),
    password  varchar(100),
    first_name varchar(40),
    last_name  varchar(17),
    role      varchar(15)
);

INSERT INTO users (email, password, first_name, last_name, role) values ('ana@gmail.com', '$2a$10$4whrrVgEPVCKOuLG754qPew4C6cZQ8giY2zS0brl7liF2eSC7cMs6
', 'Ana', 'Banana', '1');