CREATE TABLE book_rent
(
    id          serial PRIMARY KEY,
    date_rented date,
    due_date date,
    returned boolean,
    book_id serial,
     foreign key (book_id) references book(id),
    user_id serial,
     foreign key (user_id) references users(id)

);

alter table book
add column max_days_rented int,
add column no_of_copies int,
add column image varchar(64);

INSERT INTO book (author, title, isbn, genre, publisher, no_of_copies, max_days_rented) values ('Lil Baby', 'Idontknoo', '123-122-34', 'drama', 'publll', 30, 30);