CREATE TABLE book (
    id serial PRIMARY KEY,
    title varchar(40),
    genre varchar(40),
    author varchar(40),
    isbn varchar(17),
    publisher varchar(30)
);
