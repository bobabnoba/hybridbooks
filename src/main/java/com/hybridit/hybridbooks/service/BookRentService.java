package com.hybridit.hybridbooks.service;

import com.hybridit.hybridbooks.model.Book;
import com.hybridit.hybridbooks.model.BookRent;
import com.hybridit.hybridbooks.model.User;

import java.util.List;

public interface BookRentService {
    BookRent rent(Book book, User user);

    BookRent returnBook(Long bookRentId);

    BookRent getById(Long id);

    List<BookRent> getOverdueRents();

    Book getMostRentedBook();
}
