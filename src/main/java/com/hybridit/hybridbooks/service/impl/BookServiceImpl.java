package com.hybridit.hybridbooks.service.impl;

import com.hybridit.hybridbooks.exception.BookAlreadyRentedException;
import com.hybridit.hybridbooks.exception.BookNotFoundException;
import com.hybridit.hybridbooks.model.Book;
import com.hybridit.hybridbooks.repository.BookRepository;
import com.hybridit.hybridbooks.service.BookService;

import com.hybridit.hybridbooks.util.FileUploadUtil;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class BookServiceImpl implements BookService {
    private final BookRepository repository;

    public BookServiceImpl(final BookRepository bookRepository) {
        this.repository = bookRepository;
    }

    @Override
    public Book getById(Long id) {
        return repository.findById(id).orElseThrow(() -> new BookNotFoundException("Book with id " +  id + " does not exist."));
    }

    @Override
    public Book create(Book newBook) {
        return repository.save(newBook);
    }

    @Override
    public Book update(Book updatedBook) {
        if (!repository.existsById(updatedBook.getId())) {
            throw new BookNotFoundException("Book with id " +  updatedBook.getId() + " does not exist.");
        }
        return repository.save(updatedBook);
    }

    @Override
    public void delete(Long id) {
        Book found = getById(id);
        if ( repository.getNoOfRentedCopies(id) > 0) {
            throw new BookAlreadyRentedException("This book is already rented, therefore cannot be deleted.");
        }
        repository.delete(found);
    }

    @Override
    public List<Book> getAll() {
        return repository.findAll();
    }

    @Override
    public void updateImageName(Long bookId, String fileName) {
        Book book = getById(bookId);
        book.setImage(fileName);
        repository.save(book);
    }

}
