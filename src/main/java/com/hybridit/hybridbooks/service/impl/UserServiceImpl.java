package com.hybridit.hybridbooks.service.impl;

import com.hybridit.hybridbooks.exception.UserNotFoundException;
import com.hybridit.hybridbooks.model.Role;
import com.hybridit.hybridbooks.model.User;
import com.hybridit.hybridbooks.repository.UserRepository;
import com.hybridit.hybridbooks.service.UserService;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService, UserDetailsService {

    private final UserRepository repository;
    private final PasswordEncoder passwordEncoder;

    public UserServiceImpl(final UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.repository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }


    @Override
    public User getById(Long id) {
        return repository.findById(id).orElseThrow(() -> new UserNotFoundException("User with id " + id + " does not exist."));
    }

    @Override
    public List<User> getAll() {
        return repository.findAll();
    }

    @Override
    public User create(User newUser, Role role) {
        newUser.setRole(role);
        newUser.setPassword(passwordEncoder.encode(newUser.getPassword()));
        return repository.save(newUser);
    }

    @Transactional(readOnly = true)
    @Override
    public void delete(Long id) {
        User found = getById(id);
        repository.delete(found);
    }

    @Override
    public User update(User updatedUser) {
        if (!repository.existsById(updatedUser.getId())) {
            throw new UserNotFoundException("User with id " + updatedUser.getId() + " does not exist.");
        }
        return repository.save(updatedUser);
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Optional<User> userRes = repository.findByEmail(email);
        if (userRes.isEmpty())
            throw new UsernameNotFoundException("Could not find user with email = " + email);
        User user = userRes.get();
        return new org.springframework.security.core.userdetails.User(
                email,
                user.getPassword(),
                Collections.singletonList(new SimpleGrantedAuthority(user.getRole().name())));
    }
}
