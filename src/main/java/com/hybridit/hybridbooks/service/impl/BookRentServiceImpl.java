package com.hybridit.hybridbooks.service.impl;

import com.hybridit.hybridbooks.exception.BookRentNotFoundException;
import com.hybridit.hybridbooks.exception.NoAvailableCopiesException;
import com.hybridit.hybridbooks.model.Book;
import com.hybridit.hybridbooks.model.BookRent;
import com.hybridit.hybridbooks.model.User;
import com.hybridit.hybridbooks.repository.BookRentRepository;
import com.hybridit.hybridbooks.service.BookRentService;
import com.hybridit.hybridbooks.service.BookService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Service
public class BookRentServiceImpl implements BookRentService {

    private final BookRentRepository repository;
    private final BookService bookService;

    public BookRentServiceImpl(BookRentRepository repository, BookService bookService) {
        this.repository = repository;
        this.bookService = bookService;
    }

    @Transactional
    @Override
    public BookRent rent(Book book, User user) {

        if (book.getNoOfCopies() < 1) {
            throw new NoAvailableCopiesException("There are no available copies of book " + book.getTitle());
        }
        BookRent bookRent = new BookRent(user, book, LocalDate.now());
        book.setNoOfCopies(book.getNoOfCopies() - 1);
        bookService.update(book);
        return repository.save(bookRent);
    }

    @Transactional
    @Override
    public BookRent returnBook(Long rentId) {
        BookRent rent = getById(rentId);
        rent.setReturned(true);
        Book book = rent.getBook();
        book.setNoOfCopies(book.getNoOfCopies() + 1);
        bookService.update(book);
        return repository.save(rent);
    }

    @Override
    public BookRent getById(Long id) {
        return repository.findById(id).orElseThrow(() -> new BookRentNotFoundException("Book rent with id " + id + "does not exist."));
    }

    @Override
    public List<BookRent> getOverdueRents() {
        return repository.getOverdueBookRents();
    }

    @Override
    public Book getMostRentedBook() {
        return repository.getMostRentedBook().get(0);
    }
}
