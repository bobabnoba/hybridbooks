package com.hybridit.hybridbooks.service;

import com.hybridit.hybridbooks.model.Role;
import com.hybridit.hybridbooks.model.User;

import java.util.List;

public interface UserService {
    User getById(Long id);

    public List<User> getAll();

    User create(User newUser, Role role);

    void delete(Long id);

    User update(User updatedUser);
}
