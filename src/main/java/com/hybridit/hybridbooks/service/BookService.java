package com.hybridit.hybridbooks.service;

import com.hybridit.hybridbooks.model.Book;

import java.util.List;

public interface BookService {

    Book getById(Long id);

    Book create(Book newBook);

    Book update(Book updatedBook);

    void delete(Long id);

    List<Book> getAll();

    void updateImageName(Long bookId, String fileName);
}
