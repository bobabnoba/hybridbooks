package com.hybridit.hybridbooks.repository;

import com.hybridit.hybridbooks.model.Book;
import com.hybridit.hybridbooks.model.BookRent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BookRentRepository extends JpaRepository<BookRent, Long> {

    @Query("select br from BookRent br where  br.returned = false and br.dueDate < current_date ")
    List<BookRent> getOverdueBookRents();

    @Query("SELECT b " +
            "FROM Book b, BookRent br" +
            " WHERE b.id = br.book.id " +
            "GROUP BY b.id " +
            "ORDER BY COUNT(b.id) DESC")
    List<Book> getMostRentedBook();
}
