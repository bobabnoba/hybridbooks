package com.hybridit.hybridbooks.repository;

import com.hybridit.hybridbooks.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface BookRepository extends JpaRepository<Book, Long> {
    @Query("select count(b.id)" +
            " from BookRent br, Book b " +
            "where br.book.id = b.id and b.id = ?1 " +
            "group by b.id")
    int getNoOfRentedCopies(Long bookId);

}
