package com.hybridit.hybridbooks.controller;

import com.hybridit.hybridbooks.dto.LoginDto;
import com.hybridit.hybridbooks.dto.UserRequestDto;
import com.hybridit.hybridbooks.dto.UserResponseDto;
import com.hybridit.hybridbooks.exception.InvalidCredentialsException;
import com.hybridit.hybridbooks.mapper.UserMapper;
import com.hybridit.hybridbooks.model.Role;
import com.hybridit.hybridbooks.model.User;
import com.hybridit.hybridbooks.security.JWTUtil;
import com.hybridit.hybridbooks.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

    private final JWTUtil jwtUtil;
    private final AuthenticationManager authenticationManager;

    public AuthController(JWTUtil jwtUtil, AuthenticationManager authenticationManager) {
        this.jwtUtil = jwtUtil;
        this.authenticationManager = authenticationManager;
    }
    @PostMapping("/login")
    public Map<String, Object> loginHandler(@RequestBody LoginDto body) {
        try {
            UsernamePasswordAuthenticationToken authInputToken =
                    new UsernamePasswordAuthenticationToken(body.getEmail(), body.getPassword());

            Authentication auth  = authenticationManager.authenticate(authInputToken);

            String token = jwtUtil.generateToken(body.getEmail(), (List<SimpleGrantedAuthority>) auth.getAuthorities());

            return Collections.singletonMap("jwt-token", token);
        } catch (AuthenticationException e) {
            throw new InvalidCredentialsException("Invalid login credentials.");
        }
    }
}