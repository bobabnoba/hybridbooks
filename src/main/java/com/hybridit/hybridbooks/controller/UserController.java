package com.hybridit.hybridbooks.controller;

import com.hybridit.hybridbooks.dto.UserRequestDto;
import com.hybridit.hybridbooks.dto.UserResponseDto;
import com.hybridit.hybridbooks.mapper.UserMapper;
import com.hybridit.hybridbooks.model.Role;
import com.hybridit.hybridbooks.model.User;
import com.hybridit.hybridbooks.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.data.jpa.domain.AbstractPersistable_.id;

@RestController
@RequestMapping("/api/users")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping
    public ResponseEntity<List<UserResponseDto>> findAll() {
        List<User> found = userService.getAll();
        List<UserResponseDto> foundDto = found.stream()
                .map(user -> UserMapper.toResponse(user))
                .collect(Collectors.toList());
        return ResponseEntity.ok(foundDto);
    }

    @PostMapping("/register")
    public ResponseEntity<UserResponseDto> register(@RequestBody UserRequestDto newUser){
        User created = userService.create(UserMapper.fromRequest(newUser), Role.ROLE_USER);
        return ResponseEntity.ok(UserMapper.toResponse(created));
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        userService.delete(id);
        return ResponseEntity.ok().build();
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping("/{id}")
    public ResponseEntity<UserResponseDto> update(@PathVariable Long id, @RequestBody UserRequestDto updatedUser){
        User toUpdate = UserMapper.fromUpdateRequest(id, updatedUser);
        User updated = userService.update(toUpdate);
        return ResponseEntity.ok(UserMapper.toResponse(updated));
    }
}
