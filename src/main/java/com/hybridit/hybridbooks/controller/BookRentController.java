package com.hybridit.hybridbooks.controller;

import com.hybridit.hybridbooks.dto.BookDto;
import com.hybridit.hybridbooks.dto.RentBookDto;
import com.hybridit.hybridbooks.dto.RentResponseDto;
import com.hybridit.hybridbooks.mapper.BookMapper;
import com.hybridit.hybridbooks.mapper.BookRentMapper;
import com.hybridit.hybridbooks.model.Book;
import com.hybridit.hybridbooks.model.BookRent;
import com.hybridit.hybridbooks.model.User;
import com.hybridit.hybridbooks.service.BookRentService;
import com.hybridit.hybridbooks.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/rents")
public class BookRentController {

    private final BookRentService bookRentService;
    private final UserService userService;

    public BookRentController(BookRentService bookRentService, UserService userService) {
        this.bookRentService = bookRentService;
        this.userService = userService;
    }

    @PreAuthorize("hasAnyRole('ADMIN', 'USER')")
    @PostMapping("/rent")
    public ResponseEntity<RentResponseDto> rentABook(@RequestBody RentBookDto rentBookDto) {
        Book book = BookMapper.mapFromBookResponseDtoToEntity(rentBookDto.book);
        User user = userService.getById(rentBookDto.user.getId());
        BookRent bookRent = bookRentService.rent(book, user);
        RentResponseDto responseDto = BookRentMapper.mapToRentResponse(bookRent);
        return ResponseEntity.ok(responseDto);
    }

    @PreAuthorize("hasAnyRole('ADMIN', 'USER')")
    @PostMapping("/return/{rentId}")
    public ResponseEntity<RentResponseDto> returnABook(@PathVariable Long rentId) {
        BookRent rent = bookRentService.returnBook(rentId);
        RentResponseDto responseDto = BookRentMapper.mapToRentResponse(rent);
        return ResponseEntity.ok(responseDto);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/overdue-rents")
    public ResponseEntity<List<RentResponseDto>> getOverdueRents() {
        List<BookRent> rents = bookRentService.getOverdueRents();
        return ResponseEntity.ok(rents.stream()
                .map(r -> BookRentMapper.mapToRentResponse(r))
                .collect(Collectors.toList()));
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/most-rented-book")
    public ResponseEntity<BookDto> getMostRentedBook() {
        Book book = bookRentService.getMostRentedBook();
        return ResponseEntity.ok(BookMapper.mapFromEntityToBookDto(book));
    }

}
