package com.hybridit.hybridbooks.controller;

import com.hybridit.hybridbooks.dto.UserRequestDto;
import com.hybridit.hybridbooks.dto.UserResponseDto;
import com.hybridit.hybridbooks.mapper.UserMapper;
import com.hybridit.hybridbooks.model.Role;
import com.hybridit.hybridbooks.model.User;
import com.hybridit.hybridbooks.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/admin")
public class AdminController {

    private final UserService userService;

    public AdminController(UserService userService) {
        this.userService = userService;
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/register")
    public ResponseEntity<UserResponseDto> register(@RequestBody UserRequestDto user) {
        User created = userService.create(UserMapper.fromRequest(user), Role.ROLE_ADMIN);
        return ResponseEntity.ok(UserMapper.toResponse(created));
    }
}
