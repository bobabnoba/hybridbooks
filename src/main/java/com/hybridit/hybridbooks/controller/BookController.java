package com.hybridit.hybridbooks.controller;

import com.hybridit.hybridbooks.dto.BookDto;
import com.hybridit.hybridbooks.dto.CreateBookDto;
import com.hybridit.hybridbooks.mapper.BookMapper;
import com.hybridit.hybridbooks.model.Book;
import com.hybridit.hybridbooks.service.BookService;
import com.hybridit.hybridbooks.util.FileUploadUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/api/books")
public class BookController {
    private final BookService bookService;
    private final BookMapper bookMapper;


    public BookController(BookService bookService, BookMapper bookMapper) {
        this.bookService = bookService;
        this.bookMapper = bookMapper;
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping
    public ResponseEntity<BookDto> create(@Valid @RequestBody CreateBookDto newBookDto) {
        Book newBook = bookMapper.mapFromCreateDtoToEntity(newBookDto);
        Book createdBook = bookService.create(newBook);
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(createdBook.getId())
                .toUri();
        return ResponseEntity.created(location)
                .body(bookMapper.mapFromEntityToBookDto(createdBook));
    }

    @PreAuthorize("hasAnyRole('ADMIN', 'USER')")
    @GetMapping
    public ResponseEntity<List<BookDto>> findAll() {
        List<Book> found = bookService.getAll();
        List<BookDto> foundDto = found.stream()
                .map(book -> bookMapper.mapFromEntityToBookDto(book))
                .collect(Collectors.toList());
        return ResponseEntity.ok(foundDto);
    }


    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable Long id) {
        bookService.delete(id);
        return ResponseEntity.ok().build();
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping
    public ResponseEntity<BookDto> update(@Valid @RequestBody BookDto updatedDto) {
        Book toUpdate = bookMapper.mapFromBookDtoToEntity(updatedDto);
        Book updated = bookService.update(toUpdate);
        return ResponseEntity.ok(bookMapper.mapFromEntityToBookDto(updated));
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/upload-image/{bookId}")
    public ResponseEntity<BookDto> uploadPhoto(@RequestParam("image") MultipartFile multipartFile, @PathVariable Long bookId) throws IOException {

        String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
        bookService.updateImageName(bookId, fileName);

        String uploadDir = "books-images/" + bookId;
        FileUploadUtil.saveFile(uploadDir, fileName, multipartFile);

        return ResponseEntity.ok().build();
    }


}
