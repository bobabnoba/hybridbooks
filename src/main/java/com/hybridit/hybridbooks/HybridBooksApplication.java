package com.hybridit.hybridbooks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HybridBooksApplication {

	public static void main(String[] args) {
		SpringApplication.run(HybridBooksApplication.class, args);
	}

}
