package com.hybridit.hybridbooks.model;

public enum Role {
    ROLE_USER,
    ROLE_ADMIN
}
