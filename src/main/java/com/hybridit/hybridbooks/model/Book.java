package com.hybridit.hybridbooks.model;

import javax.persistence.*;

@Entity
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String author;
    private String title;
    @Column(unique = true)
    private String isbn;
    private String genre;
    private String publisher;
    private Integer noOfCopies;
    private Integer maxDaysRented;

    @Column(length = 64)
    private String image;

    private Book() {
    }

    private Book(Long id, String author, String title, String ISBN, String genre, String publisher) {
        this.id = id;
        this.author = author;
        this.title = title;
        this.isbn = ISBN;
        this.genre = genre;
        this.publisher = publisher;
    }

    public Long getId() {
        return id;
    }

    public String getAuthor() {
        return author;
    }

    public String getTitle() {
        return title;
    }

    public String getIsbn() {
        return isbn;
    }

    public String getGenre() {
        return genre;
    }

    public String getPublisher() {
        return publisher;
    }
    public Integer getNoOfCopies() { return noOfCopies; }
    public Integer getMaxDaysRented() {return maxDaysRented; }

    public void setNoOfCopies(Integer noOfCopies) { this.noOfCopies = noOfCopies; }
    public void setImage(String image) { this.image = image; }
    public static class BookBuilder {

        private Long id;
        private String author;
        private String title;
        private String ISBN;
        private String genre;
        private String publisher;
        private Integer noOfCopies;
        private Integer maxDaysRented;
        private String image;

        public BookBuilder(String author, String title, String ISBN, String genre, String publisher) {
            this.author = author;
            this.title = title;
            this.ISBN = ISBN;
            this.genre = genre;
            this.publisher = publisher;
        }

        public BookBuilder withImage(String image) {
            this.image = image;
            return this;
        }

        public BookBuilder withId(Long id) {
            this.id = id;
            return this;
        }

        public BookBuilder withNoOfCopies(Integer noOfCopies) {
            this.noOfCopies = noOfCopies;
            return this;
        }

        public BookBuilder withMaxDaysRented(Integer maxDaysRented){
            this.maxDaysRented = maxDaysRented;
            return this;
        }

        public Book build() {
            Book book = new Book();
            book.id = this.id;
            book.author = this.author;
            book.title = this.title;
            book.isbn = this.ISBN;
            book.genre = this.genre;
            book.publisher = this.publisher;
            book.noOfCopies = this.noOfCopies;
            book.maxDaysRented = this.maxDaysRented;
            book.image = this.image;
            return book;
        }

    }
}

