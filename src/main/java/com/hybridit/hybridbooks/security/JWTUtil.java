package com.hybridit.hybridbooks.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class JWTUtil {

    @Value("${jwt.secret}")
    private String secret;
    private final String APP_NAME = "hybrid-internship-program";
    @Value("${jwt.expires_in}")
    private int expiresIn;

    public String generateToken(String email, List<SimpleGrantedAuthority> authorities) throws IllegalArgumentException, JWTCreationException {
        return JWT.create()
                .withSubject("user")
                .withClaim("email", email)
                .withClaim("role", authorities.stream()
                        .map(auth -> auth.toString()).collect(Collectors.toList()))
                .withIssuedAt(new Date())
                .withIssuer(APP_NAME)
                .withExpiresAt(generateExpirationDate())
                .sign(Algorithm.HMAC256(secret));
    }

    public DecodedJWT validateTokenAndRetrieveSubject(String token)throws JWTVerificationException {
        JWTVerifier verifier = JWT.require(Algorithm.HMAC256(secret))
                .withSubject("user")
                .withIssuer(APP_NAME)
                .build();
        DecodedJWT jwt = verifier.verify(token);
        return jwt;
    }

    private Date generateExpirationDate() {
        return new Date(new Date().getTime() + expiresIn);
    }
}
