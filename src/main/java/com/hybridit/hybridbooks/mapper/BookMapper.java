package com.hybridit.hybridbooks.mapper;

import com.hybridit.hybridbooks.dto.BookResponseDto;
import com.hybridit.hybridbooks.dto.CreateBookDto;
import com.hybridit.hybridbooks.dto.BookDto;
import com.hybridit.hybridbooks.model.Book;
import org.springframework.stereotype.Component;

@Component
public class BookMapper {

    public static Book mapFromCreateDtoToEntity(CreateBookDto bookDto) {

        Book book = new Book.BookBuilder(bookDto.getAuthor(), bookDto.getTitle(), bookDto.getISBN(), bookDto.getGenre(), bookDto.getPublisher())
                .build();
        return book;
    }

    public static Book mapFromBookDtoToEntity(BookDto bookDto){
        Book book = new Book.BookBuilder(bookDto.getAuthor(), bookDto.getTitle(), bookDto.getIsbn(), bookDto.getGenre(), bookDto.getPublisher())
                .withId(bookDto.getId())
                .build();
        return book;
    }

    public static BookDto mapFromEntityToBookDto(Book book){
        BookDto bookDto = new BookDto.BookDtoBuilder(book.getId(), book.getAuthor(), book.getTitle(), book.getIsbn(), book.getGenre(), book.getPublisher())
                .build();
        return bookDto;
    }

    public static BookResponseDto mapFromEntityToBookResponseDto(Book book) {
        BookResponseDto dto = new BookResponseDto(book.getId(), book.getAuthor(), book.getTitle(), book.getIsbn(), book.getGenre(), book.getPublisher(), book.getMaxDaysRented(), book.getNoOfCopies());
        return dto;
    }

    public static Book mapFromBookResponseDtoToEntity(BookResponseDto bookDto) {
        Book book = new Book.BookBuilder(bookDto.getAuthor(), bookDto.getTitle(), bookDto.getIsbn(), bookDto.getGenre(), bookDto.getPublisher())
                .withId(bookDto.getId())
                .withMaxDaysRented(bookDto.getMaxDaysRented())
                .withNoOfCopies(bookDto.getNoOfCopies())
                .build();
        return book;
    }
}
