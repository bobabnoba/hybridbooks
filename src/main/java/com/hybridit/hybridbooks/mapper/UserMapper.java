package com.hybridit.hybridbooks.mapper;

import com.hybridit.hybridbooks.dto.UserRequestDto;
import com.hybridit.hybridbooks.dto.UserResponseDto;
import com.hybridit.hybridbooks.model.User;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    public static User fromRequest(UserRequestDto dto){
        User user = new User(dto.getEmail(), dto.getPassword(), dto.getFirstName(), dto.getLastName());
        return user;
    }

    public static User fromUpdateRequest(Long id, UserRequestDto dto){
        User user = new User(id, dto.getEmail(), dto.getPassword(), dto.getFirstName(), dto.getLastName());
        return user;
    }

    public static UserResponseDto toResponse(User user){
        UserResponseDto dto = new UserResponseDto(user.getId(), user.getEmail(), user.getFirstName(), user.getLastName());
        return dto;
    }

    public static User fromResponse(UserResponseDto user){
        return new User(user.getId(), user.getEmail(), user.getFirstName(), user.getLastName());
    }
}
