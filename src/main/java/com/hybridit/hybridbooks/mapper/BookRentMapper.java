package com.hybridit.hybridbooks.mapper;

import com.hybridit.hybridbooks.dto.RentResponseDto;
import com.hybridit.hybridbooks.model.BookRent;
import org.springframework.stereotype.Component;

@Component
public class BookRentMapper {

    public static RentResponseDto mapToRentResponse(BookRent bookRent){
        return new RentResponseDto(bookRent.getId(), BookMapper.mapFromEntityToBookResponseDto(bookRent.getBook()),
                UserMapper.toResponse(bookRent.getUser()), bookRent.getDateRented(), bookRent.isReturned(), bookRent.getDueDate());
    }
}
