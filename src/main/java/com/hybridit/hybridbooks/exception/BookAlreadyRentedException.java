package com.hybridit.hybridbooks.exception;

public class BookAlreadyRentedException extends  RuntimeException {

    public BookAlreadyRentedException(String message) {
        super(message);
    }
}
