package com.hybridit.hybridbooks.exception.handler;

import com.hybridit.hybridbooks.exception.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@ControllerAdvice
public class GlobalControllerExceptionHandler {

    @ExceptionHandler(BookNotFoundException.class)
    protected ResponseEntity<ApiError> handleBookNotFound(BookNotFoundException exception, HttpServletRequest request) {
        HttpStatus status = HttpStatus.NOT_FOUND;
        return ResponseEntity.status(status).body(new ApiError(status, exception.getMessage(), request.getServletPath()));
    }

    @ExceptionHandler(UserNotFoundException.class)
    protected ResponseEntity<ApiError> handleUserNotFound(UserNotFoundException exception, HttpServletRequest request) {
        HttpStatus status = HttpStatus.NOT_FOUND;
        return ResponseEntity.status(status).body(new ApiError(status, exception.getMessage(), request.getServletPath()));
    }

    @ExceptionHandler(BookAlreadyRentedException.class)
    protected ResponseEntity<ApiError> handleDeletingRentedBook(BookAlreadyRentedException exception, HttpServletRequest request) {
        HttpStatus status = HttpStatus.BAD_REQUEST;
        return ResponseEntity.status(status).body(new ApiError(status, exception.getMessage(), request.getServletPath()));
    }

    @ExceptionHandler(IOException.class)
    protected ResponseEntity<ApiError> handleIO(IOException exception, HttpServletRequest request) {
        HttpStatus status = HttpStatus.FORBIDDEN;
        return ResponseEntity.status(status).body(new ApiError(status, exception.getMessage(), request.getServletPath()));
    }
    @ExceptionHandler(NoAvailableCopiesException.class)
    protected ResponseEntity<ApiError> handleNoAvailableCopies(NoAvailableCopiesException exception, HttpServletRequest request) {
        HttpStatus status = HttpStatus.NOT_FOUND;
        return ResponseEntity.status(status).body(new ApiError(status, exception.getMessage(), request.getServletPath()));
    }

    @ExceptionHandler(BookRentNotFoundException.class)
    protected ResponseEntity<ApiError> handleNoBookRent(BookRentNotFoundException exception, HttpServletRequest request) {
        HttpStatus status = HttpStatus.NOT_FOUND;
        return ResponseEntity.status(status).body(new ApiError(status, exception.getMessage(), request.getServletPath()));
    }

    @ExceptionHandler(InvalidCredentialsException.class)
    protected ResponseEntity<ApiError> handleInvalidCredentials(InvalidCredentialsException exception, HttpServletRequest request) {
        HttpStatus status = HttpStatus.BAD_REQUEST;
        return ResponseEntity.status(status).body(new ApiError(status, exception.getMessage(), request.getServletPath()));
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    protected ResponseEntity<ApiError> handleMethodArgNotValid(MethodArgumentNotValidException exception, HttpServletRequest request) {
        HttpStatus status = HttpStatus.BAD_REQUEST;
        StringBuilder s = new StringBuilder();
        exception.getAllErrors().forEach(err -> {
            s.append(err.getDefaultMessage());
            s.append("! ");
        });
        return ResponseEntity.status(status).body(new ApiError(status, s.toString(), request.getServletPath()));
    }
}