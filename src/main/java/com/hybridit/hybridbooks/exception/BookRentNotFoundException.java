package com.hybridit.hybridbooks.exception;

public class BookRentNotFoundException extends RuntimeException {
    public BookRentNotFoundException(String message) {
        super(message);
    }
}
