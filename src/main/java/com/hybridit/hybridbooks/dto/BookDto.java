package com.hybridit.hybridbooks.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import static com.hybridit.hybridbooks.dto.Constants.ISBN_REGEX;

public class BookDto {

    @NotNull(message = "ID cannot be null")
    private Long id;
    @NotBlank(message = "Author is mandatory")
    private String author;
    @NotBlank(message = "Title is mandatory")
    private String title;
    @Pattern(regexp = ISBN_REGEX, message = "Invalid ISBN format")
    @NotBlank(message = "ISBN is mandatory")
    private String isbn;
    @NotBlank(message = "Genre is mandatory")
    private String genre;
    @NotBlank(message = "Publisher is mandatory")
    private String publisher;

    private BookDto() {
    }

    public Long getId() {
        return id;
    }

    public String getAuthor() {
        return author;
    }

    public String getTitle() {
        return title;
    }

    public String getIsbn() {
        return isbn;
    }

    public String getGenre() {
        return genre;
    }

    public String getPublisher() {
        return publisher;
    }

    public static class BookDtoBuilder {

        private Long id;
        private String author;
        private String title;
        private String ISBN;
        private String genre;
        private String publisher;

        public BookDtoBuilder(Long id, String author, String title, String ISBN, String genre, String publisher) {
            this.id = id;
            this.author = author;
            this.title = title;
            this.ISBN = ISBN;
            this.genre = genre;
            this.publisher = publisher;
        }

        public BookDto build() {
            BookDto dto = new BookDto();
            dto.id = id;
            dto.author = author;
            dto.title = title;
            dto.isbn = ISBN;
            dto.genre = genre;
            dto.publisher = publisher;
            return dto;
        }
    }
}
