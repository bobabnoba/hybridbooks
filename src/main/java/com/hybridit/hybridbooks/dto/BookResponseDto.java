package com.hybridit.hybridbooks.dto;

public class BookResponseDto {

    public Long id;
    public String author;
    public String title;
    public String isbn;
    public String genre;
    public String publisher;
    public Integer maxDaysRented;
    public Integer noOfCopies;

    public BookResponseDto(Long id, String author, String title, String isbn, String genre, String publisher, Integer maxDaysRented, Integer noOfCopies) {
        this.id = id;
        this.author = author;
        this.title = title;
        this.isbn = isbn;
        this.genre = genre;
        this.publisher = publisher;
        this.maxDaysRented = maxDaysRented;
        this.noOfCopies = noOfCopies;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public Integer getMaxDaysRented() {
        return maxDaysRented;
    }

    public void setMaxDaysRented(Integer maxDaysRented) {
        this.maxDaysRented = maxDaysRented;
    }

    public Integer getNoOfCopies() {
        return noOfCopies;
    }

    public void setNoOfCopies(Integer noOfCopies) {
        this.noOfCopies = noOfCopies;
    }
}
