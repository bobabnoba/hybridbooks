package com.hybridit.hybridbooks.dto;

import java.time.LocalDate;

public class RentResponseDto {

    public Long id;
    public BookResponseDto book;
    public UserResponseDto user;
    public LocalDate dateRented;
    public LocalDate dueDate;
    public Boolean returned;

    public RentResponseDto(Long id, BookResponseDto book, UserResponseDto user, LocalDate dateRented, Boolean isReturned, LocalDate due) {
        this.book = book;
        this.id = id;
        this.user = user;
        this.dueDate = due;
        this.dateRented = dateRented;
        this.returned = isReturned;
    }

    public BookResponseDto getBook() {
        return book;
    }

    public void setBook(BookResponseDto book) {
        this.book = book;
    }

    public UserResponseDto getUser() {
        return user;
    }

    public void setUser(UserResponseDto user) {
        this.user = user;
    }

    public LocalDate getDateRented() {
        return dateRented;
    }

    public void setDateRented(LocalDate dateRented) {
        this.dateRented = dateRented;
    }
}
