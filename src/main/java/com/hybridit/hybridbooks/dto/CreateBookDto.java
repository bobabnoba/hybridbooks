package com.hybridit.hybridbooks.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import static com.hybridit.hybridbooks.dto.Constants.ISBN_REGEX;

public class CreateBookDto {

    @NotBlank(message = "Author is mandatory")
    private String author;
    @NotBlank(message = "Title is mandatory")
    private String title;
    @Pattern(regexp = ISBN_REGEX, message = "Invalid ISBN format")
    @NotBlank(message = "ISBN is mandatory")
    private String ISBN;
    @NotBlank(message = "Genre is mandatory")
    private String genre;
    @NotBlank(message = "Publisher is mandatory")
    private String publisher;

    public CreateBookDto(String author, String title, String ISBN, String genre, String publisher) {
        this.author = author;
        this.title = title;
        this.ISBN = ISBN;
        this.genre = genre;
        this.publisher = publisher;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getISBN() {
        return ISBN;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }
}
