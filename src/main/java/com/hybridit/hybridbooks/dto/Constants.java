package com.hybridit.hybridbooks.dto;

public class Constants {
    public static final String ISBN_REGEX = "^(?=(?:\\D*\\d){10}(?:(?:\\D*\\d){3})?$)[\\d-]+$";
}
