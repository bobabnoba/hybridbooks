package com.hybridit.hybridbooks.dto;

public class RentBookDto {

    public UserResponseDto user;
    public BookResponseDto book;

    public RentBookDto(UserResponseDto user, BookResponseDto book) {
        this.user = user;
        this.book = book;
    }
}
