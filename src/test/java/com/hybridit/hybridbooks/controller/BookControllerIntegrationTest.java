package com.hybridit.hybridbooks.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hybridit.hybridbooks.dto.BookDto;
import com.hybridit.hybridbooks.dto.CreateBookDto;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc()
@ActiveProfiles("test")
class BookControllerIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Test
    @WithMockUser(roles = {"ADMIN", "USER"})
    void shoud_get_all_books() throws Exception {

        mvc.perform(MockMvcRequestBuilders
                        .get("/api/books")
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[*].id").isNotEmpty());
    }

    @Test
    @WithMockUser(roles = {"ADMIN"})
    void should_create_new_book() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                        .post("/api/books")
                        .content(asJsonString(new CreateBookDto("author", "title", "978-3-16-148410-5", "genre", "publisher")))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists());
    }

    @Test
    @WithMockUser(roles = {"ADMIN"})
    public void should_delete_book() throws Exception {
        mvc.perform( MockMvcRequestBuilders.delete("/api/books/{id}", 1) )
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(roles = {"ADMIN"})
    public void should_update_book() throws Exception
    {
        mvc.perform( MockMvcRequestBuilders
                        .put("/api/books/")
                        .content(asJsonString(new BookDto.BookDtoBuilder(2L, "tauthor222", "ttitle22", "978-3-16-148410-0", "tgenre2", "tpublisher2").build()))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.author").value("tauthor222"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.title").value("ttitle22"));
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}