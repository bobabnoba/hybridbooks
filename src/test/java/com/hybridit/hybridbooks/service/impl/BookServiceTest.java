package com.hybridit.hybridbooks.service.impl;

import com.hybridit.hybridbooks.exception.BookNotFoundException;
import com.hybridit.hybridbooks.model.Book;
import com.hybridit.hybridbooks.repository.BookRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class BookServiceImplTest {
    @Mock
    private BookRepository bookRepository;
    @InjectMocks
    private BookServiceImpl bookService;

    @Test
    public void should_return_updated_book_on_book_update() {


        Book updatedBook = new Book.BookBuilder("author_updated", "title_updated", "978-3-16-148410-1", "genre", "publisher")
                .withId(1L)
                .build();

        Mockito.when(bookRepository.existsById(1L)).thenReturn(true);
        Mockito.when(bookRepository.save(updatedBook)).thenReturn(updatedBook);

        Book book = bookService.update(updatedBook);

        Mockito.verify(bookRepository, Mockito.times(1)).existsById(1L);
        Mockito.verify(bookRepository, Mockito.times(1)).save(updatedBook);

        assertEquals(book.getAuthor(), updatedBook.getAuthor());
        assertEquals(book.getTitle(), updatedBook.getTitle());
    }

    @Test
    public void should_throw_exception_on_book_update() {
        Book book = new Book.BookBuilder("author", "title", "978-3-16-148410-1", "genre", "publisher")
                .withId(2L)
                .build();
        Mockito.when(bookRepository.existsById(book.getId())).thenReturn(false);

        assertThrows(BookNotFoundException.class, () -> bookService.update(book));
        Mockito.verify(bookRepository, Mockito.never()).save(book);
        Mockito.verify(bookRepository, Mockito.times(1)).existsById(book.getId());
    }

    @Test
    public void should_delete_book_on_book_delete() {

        Book book = new Book.BookBuilder("author_updated", "title_updated", "978-3-16-148410-1", "genre", "publisher")
                .withId(1L)
                .build();

        Mockito.when(bookRepository.findById(book.getId())).thenReturn(Optional.of(book));

        bookService.delete(book.getId());

        Mockito.verify(bookRepository).findById(book.getId());
        Mockito.verify(bookRepository).delete(book);
    }

    @Test
    public void shoud_throw_exception_on_book_delete() {
        Book book = new Book.BookBuilder("author_updated", "title_updated", "978-3-16-148410-1", "genre", "publisher")
                .withId(1L)
                .build();

        Mockito.when(bookRepository.findById(book.getId())).thenReturn(Optional.empty());
        assertThrows(BookNotFoundException.class, () -> bookService.delete(book.getId()));

    }
}